package by.blackpearl.userinformer;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import java.util.Locale;

/**
 * Created by yauheni.
 */

public class DatepickerView extends LinearLayout {

    private Context mContext;
    private DatePicker mDatepicker;
    private IDatepickerCallback mCallback;

    public DatepickerView(Context context, IDatepickerCallback callback) {
        super(context);
        mContext = context;
        mCallback = callback;
        initialization();
    }

    public DatepickerView(Context context) {
        super(context);
        mContext = context;
        initialization();
    }

    public DatepickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initialization();
    }

    public DatepickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initialization();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DatepickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        initialization();
    }

    private void initialization() {
        LayoutInflater.from(mContext).inflate(R.layout.datepicker_view, this, true);
        this.mDatepicker = (DatePicker) findViewById(R.id.date_picker);
        findViewById(R.id.btn_confirm).setOnClickListener(getConfirmClickListener());
    }

    private OnClickListener getConfirmClickListener() {
        return new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback == null) {
                    return;
                }
                String s = String.format(Locale.getDefault(), "%02d", mDatepicker.getDayOfMonth()) + "." +
                        String.format(Locale.getDefault(), "%02d", mDatepicker.getMonth() + 1) + "." +
                        String.format(Locale.getDefault(), "%02d", mDatepicker.getYear());
                mCallback.onDatePicked(s);
            }
        };
    }

    public void setCallback(@Nullable IDatepickerCallback callback) {
        this.mCallback = callback;
    }

    public interface IDatepickerCallback {
        void onDatePicked(String date);
    }
}
