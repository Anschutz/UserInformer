package by.blackpearl.userinformer;

import io.realm.RealmObject;

/**
 * Created by yauheni.
 */

public class UsersInformationTable extends RealmObject {

    public static String USER_LAST_NAME = "UserLastName";
    public static String DATE_OF_BIRTH = "DateOfBirth";
    public static String TELEPHONE_NUMBER = "TelephoneNumber";

    private String UserLastName;
    private String DateOfBirth;
    private String TelephoneNumber;

    public String getUserLastName() {
        return UserLastName;
    }

    public void setUserLastName(String userLastName) {
        UserLastName = userLastName;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getTelephoneNumber() {
        return TelephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        TelephoneNumber = telephoneNumber;
    }
}
