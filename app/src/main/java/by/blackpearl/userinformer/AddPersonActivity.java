package by.blackpearl.userinformer;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;

public class AddPersonActivity extends AppCompatActivity {

    private static final String IS_DATEPICKER_FOCUSABLE = "isDatePickerFocusable";
    private TextInputEditText mLastNameEt;
    private TextInputEditText mDateBirth;
    private TextInputEditText mTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final boolean isDatepickerHasFocus = savedInstanceState != null &&
                savedInstanceState.getBoolean(IS_DATEPICKER_FOCUSABLE, false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        this.mLastNameEt = (TextInputEditText) findViewById(R.id.et_last_name);
        this.mDateBirth = (TextInputEditText) findViewById(R.id.et_date_of_birth);
        this.mTel = (TextInputEditText) findViewById(R.id.et_tel);

        mDateBirth.setOnClickListener(getDateOnClickListener());
        mDateBirth.setKeyListener(null);
        mDateBirth.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            private boolean mIsDatepickerHasFocus = isDatepickerHasFocus;

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !mIsDatepickerHasFocus) {
                    mDateBirth.performClick();
                }
                else if (mIsDatepickerHasFocus) {
                    mIsDatepickerHasFocus = false;
                }
            }
        });

        Button confirmBtn = (Button) findViewById(R.id.btn_confirm);
        confirmBtn.setOnClickListener(getBtnConfirmclickListener());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        boolean focus = mDateBirth.hasFocus();
        outState.putBoolean(IS_DATEPICKER_FOCUSABLE, focus);
        super.onSaveInstanceState(outState);
    }

    private View.OnClickListener getDateOnClickListener() {
        return new View.OnClickListener() {

            private AlertDialog dpd;

            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }

            private void showDatePickerDialog() {
                dpd = new AlertDialog.Builder(AddPersonActivity.this)
                        .setTitle(R.string.set_date_of_birth)
                        .setCancelable(true)
                        .create();
                dpd.setCustomTitle(new DatepickerView(AddPersonActivity.this,
                        getCallback()));
                dpd.show();
            }

            private DatepickerView.IDatepickerCallback getCallback() {
                return new DatepickerView.IDatepickerCallback() {
                    @Override
                    public void onDatePicked(String date) {
                        mDateBirth.setText(date);
                        if (dpd != null) {
                            dpd.hide();
                        }
                    }
                };
            }
        };
    }

    private View.OnClickListener getBtnConfirmclickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkInput()) {
                    Toast.makeText(AddPersonActivity.this, R.string.some_field_is_not_valid,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                UsersInformationTable person = realm.createObject(UsersInformationTable.class);
                person.setUserLastName(mLastNameEt.getText().toString());
                person.setDateOfBirth(mDateBirth.getText().toString());
                person.setTelephoneNumber(mTel.getText().toString());
                realm.copyToRealm(person);
                realm.commitTransaction();
                realm.close();
                setResult(RESULT_OK);
                finish();
            }
        };
    }

    private boolean checkInput() {
        if (mLastNameEt.getText().toString().equals("") ||
                mDateBirth.getText().toString().equals("") ||
                mTel.getText().toString().equals("")) {
            return false;
        }
        if (mLastNameEt.getText().toString().equals(" ") ||
                mDateBirth.getText().toString().equals(" ") ||
                mTel.getText().toString().equals(" ")) {
            return false;
        }
        if (!validDate()) {
            return false;
        }
        return true;
    }

    private boolean validDate() {
        try {
            String[] dateStr = mDateBirth.getText().toString().split("\\.");
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            if (dateStr.length != 3) {
                return false;
            }
            calendar.set(Integer.valueOf(dateStr[2]), Integer.valueOf(dateStr[1]) - 1,
                    Integer.valueOf(dateStr[0]));
            Date selectedDate = calendar.getTime();
            calendar.setTimeInMillis(System.currentTimeMillis());
            Date dateNow = calendar.getTime();
            if (!dateNow.before(selectedDate)) {
                return true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
