package by.blackpearl.userinformer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by yauheni.
 */

class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.Holder> {

    private final Context mContext;
    private RealmResults<UsersInformationTable> mUserInformationTable;

    UsersAdapter(Context context) {
        this.mContext = context;
        Realm realm = Realm.getDefaultInstance();
        this.mUserInformationTable = realm.where(UsersInformationTable.class).findAll()
                .sort(UsersInformationTable.USER_LAST_NAME);
    }

    public void notifyData() {
        notifyItemRangeChanged(0, getItemCount() + 1);
    }

    @Override
    public UsersAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.user_info_view, parent, false));
    }

    @Override
    public void onBindViewHolder(UsersAdapter.Holder holder, int position) {
        if (position == 0 && mUserInformationTable.size() == 0) {
            holder.mLastName.setText(R.string.database_is_empty);
            holder.mBirthDate.setText("");
            holder.mTelNumber.setText("");
        }
        else {
            holder.mLastName.setText(mUserInformationTable.get(position).getUserLastName());
            holder.mBirthDate.setText(mUserInformationTable.get(position).getDateOfBirth());
            holder.mTelNumber.setText(mUserInformationTable.get(position).getTelephoneNumber());
        }
    }

    @Override
    public int getItemCount() {
        if (mUserInformationTable.size() == 0) {
            return 1;
        }
        return mUserInformationTable.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        TextView mLastName;
        TextView mBirthDate;
        TextView mTelNumber;

        Holder(View itemView) {
            super(itemView);
            mLastName = (TextView) itemView.findViewById(R.id.tv_last_name);
            mBirthDate = (TextView) itemView.findViewById(R.id.tv_date_of_birth);
            mTelNumber = (TextView) itemView.findViewById(R.id.tv_tel);
        }
    }
}
