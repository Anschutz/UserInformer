package by.blackpearl.userinformer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import io.realm.Realm;

public class MainUserInformationActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 17;

    private UsersAdapter mUsersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user_information);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.information_about_users);
        setSupportActionBar(toolbar);

        Realm.init(getApplicationContext());

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_user_information);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.mUsersAdapter = new UsersAdapter(this);
        rv.setAdapter(mUsersAdapter);

        FloatingActionButton fabAddPerson= (FloatingActionButton) findViewById(R.id.fab_add_person);
        CoordinatorLayout.LayoutParams fabParams = (CoordinatorLayout.LayoutParams)
                fabAddPerson.getLayoutParams();
        fabParams.setBehavior(new FabShowHideBehavior());
        fabAddPerson.setLayoutParams(fabParams);
        fabAddPerson.setOnClickListener(getAddPersonBtnClickListener());
    }

    private View.OnClickListener getAddPersonBtnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(
                        new Intent(
                                MainUserInformationActivity.this,
                                AddPersonActivity.class
                        ),
                        REQUEST_CODE
                );
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                mUsersAdapter.notifyData();
                Toast.makeText(this, R.string.new_user_was_added, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(this, R.string.determine, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
